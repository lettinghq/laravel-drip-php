<?php
namespace LettingHQ\DripPhp\Interfaces;

interface DripPhpInterface
{
    public function get_campaigns($params);
	public function fetch_campaign($params);
	public function get_accounts();
	public function create_or_update_subscriber($params);
	public function fetch_subscriber($params);
	public function subscribe_subscriber($params);
	public function unsubscribe_subscriber($params);
	public function tag_subscriber($params);
	public function untag_subscriber($params);
	public function record_event($params);
    public function make_request($url, $params = array(), $req_method = self::GET);
	public function get_request_info();
	public function get_error_message();
    public function get_error_code();
	public function _parse_error($res);
}
