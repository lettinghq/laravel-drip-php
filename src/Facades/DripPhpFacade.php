<?php
namespace LettingHQ\DripPhp\Facades;

use Illuminate\Support\Facades\Facade as IlluminateFacade;

class DripPhpFacade extends IlluminateFacade
{
    protected static function getFacadeAccessor()
    {
        return 'dripphp';
    }
}