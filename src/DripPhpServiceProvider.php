<?php
namespace LettingHQ\DripPhp;

use Illuminate\Support\ServiceProvider;

class DripPhpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('dripphp', function($app) {
            return new DripPhp();
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/drip.php' => config_path('drip.php'),
        ]);
    }

    public function provides()
    {
        return [
            'dripphp',
        ];
    }
}
