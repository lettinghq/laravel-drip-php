# Drip API Laravel Wrapper #
## A Laravel wrapper for Drip's PHP wrapper for their REST API ##

### Installation ###

1) Add the package as a dependancy in your composer.json

```
composer require lettinghq/laravel-drip-php
```


2) publish the vendor config file
```
php artisan vendor:serve
```


3) Add your Drip API token to the config file located in app/config/drip.php. We recommend you add this key to your project .env file instead of directly adding it to your config file.
```
DRIP_API_TOKEN=your token here
```


4) Add the following line to your providers array in your config/app.php file
```
LettingHQ\DripPhp\DripPhpServiceProvider::class,
```


5) Add the following line to your aliases array in your config/app.php file
```
'DripPhp' => LettingHQ\DripPhp\DripPhp::class,
```